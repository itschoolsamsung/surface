package com.example.al.surface;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setContentView(R.layout.activity_main);
        setContentView(new DrawView(this));
    }

    public class DrawView extends SurfaceView implements SurfaceHolder.Callback {

        private DrawThread drawThread;

        public DrawView(Context context) {
            super(context);

            getHolder().addCallback(this);
        }

        @Override
        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            drawThread = new DrawThread(getContext(), getHolder());
            drawThread.start();
        }

        @Override
        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            drawThread.requestStop();
            boolean retry = true;

            while (retry) {
                try {
                    drawThread.join();
                    retry = false;
                } catch (InterruptedException e) {
                }
            }
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            drawThread.setX(event.getX());
            drawThread.setY(event.getY());
            drawThread.resetA();

			return super.onTouchEvent(event);
        }
    }

    public class DrawThread extends Thread {

        private SurfaceHolder surfaceHolder;
        private volatile boolean running = true;
        private Paint paint = new Paint();
        private float x = 100;
        private float y = 100;
        private float a = 1;

        DrawThread(Context context, SurfaceHolder surfaceHolder) {
            this.surfaceHolder = surfaceHolder;
        }

        void setX(float x) {
            this.x = x;
        }

        void setY(float y) {
            this.y = y;
        }

        void resetA() {
            this.a = 1;
        }

        void requestStop() {
            running = false;
        }

        @Override
        public void run() {
            while (running) {
                Canvas canvas = surfaceHolder.lockCanvas();
                if (canvas != null) {
                    try {
                        canvas.drawARGB(255, 0, 0, 50);
                        paint.setColor(Color.RED);
                        canvas.drawCircle(this.x, this.y, 50, paint);
                        if (this.y + 50 < canvas.getHeight()) {
                            this.y += a;
                            a += .5;
                        } else {
                            this.y = canvas.getHeight() - 50;
                        }
                    } finally {
                        surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }
    }
}
